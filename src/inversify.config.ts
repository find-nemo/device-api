import { Container } from 'inversify';

import { PluginManager } from './plugins.config';
import { InterfaceErrorHandlerPlugin } from './plugins/error-handler.interface';
import { ErrorHandlerPlugin } from './plugins/error-handler.plugin';
import { InterfaceRequestValidationMiddleware } from './plugins/request-validation.interface';
import { RequestValidationMiddleware } from './plugins/request-validation.plugin';
import { InterfaceSecretManagerPlugin } from './plugins/secret-manager.interface';
import { SecretManagerPlugin } from './plugins/secret-manager.plugin';
import { InterfaceSequelizePlugin } from './plugins/sequelize.interface';
import { SequelizePlugin } from './plugins/sequelize.plugin';
import { DeviceController } from './server/controllers/device.controller';
import { HealthController } from './server/controllers/health.controller';
import { SwaggerController } from './server/controllers/swagger.controller';
import { FirebaseTokenMiddleware } from './server/middlewares/firebase-token.middleware';
import { SYMBOLS } from './server/models/error-symbol';
import { DeviceDataAccess } from './server/repository/device.da';
import { DeviceDataAccessInterface } from './server/repository/device.interface';
import { DeviceServiceInterface } from './server/services/device.interface';
import { DeviceService } from './server/services/device.service';
import { ValidateTokenUtil } from './server/utils/validate-user-token.util';
import { UserDataAccessInterface } from './server/repository/user.interface';
import { UserDataAccess } from './server/repository/user.da';
import { UserServiceInterface } from './server/services/user.service';
import { UserService } from './server/services/user.interface';

const appContainer = new Container();
appContainer
  .bind<PluginManager>(nameof<PluginManager>())
  .to(PluginManager)
  .inSingletonScope();
appContainer
  .bind<InterfaceErrorHandlerPlugin>(nameof<InterfaceErrorHandlerPlugin>())
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  .toFactory<InterfaceErrorHandlerPlugin>((): any => {
    return (name: string): ErrorHandlerPlugin => {
      name = name || 'UNKNOWN';
      const service = appContainer.get<ErrorHandlerPlugin>(
        SYMBOLS.DiagnosticsInstance
      );
      service.setName(name);
      return service;
    };
  });
appContainer
  .bind<InterfaceErrorHandlerPlugin>(SYMBOLS.DiagnosticsInstance)
  .to(ErrorHandlerPlugin)
  .inTransientScope();
appContainer
  .bind<InterfaceRequestValidationMiddleware>(
    nameof<InterfaceRequestValidationMiddleware>()
  )
  .to(RequestValidationMiddleware)
  .inTransientScope();
appContainer
  .bind<SwaggerController>(nameof<SwaggerController>())
  .to(SwaggerController);
appContainer
  .bind<ValidateTokenUtil>(nameof<ValidateTokenUtil>())
  .to(ValidateTokenUtil);
appContainer
  .bind<HealthController>(nameof<HealthController>())
  .to(HealthController);
appContainer
  .bind<FirebaseTokenMiddleware>(nameof<FirebaseTokenMiddleware>())
  .to(FirebaseTokenMiddleware)
  .inSingletonScope();
appContainer
  .bind<InterfaceSecretManagerPlugin>(nameof<InterfaceSecretManagerPlugin>())
  .to(SecretManagerPlugin)
  .inSingletonScope();
appContainer
  .bind<InterfaceSequelizePlugin>(nameof<InterfaceSequelizePlugin>())
  .to(SequelizePlugin)
  .inSingletonScope();
appContainer
  .bind<DeviceDataAccessInterface>(nameof<DeviceDataAccessInterface>())
  .to(DeviceDataAccess);
appContainer
  .bind<DeviceServiceInterface>(nameof<DeviceServiceInterface>())
  .to(DeviceService);
appContainer
  .bind<DeviceController>(nameof<DeviceController>())
  .to(DeviceController);
appContainer
  .bind<UserDataAccessInterface>(nameof<UserDataAccessInterface>())
  .to(UserDataAccess);
appContainer
  .bind<UserServiceInterface>(nameof<UserServiceInterface>())
  .to(UserService);
export { appContainer };
