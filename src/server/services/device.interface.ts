import { Device } from '../models/device';

export interface DeviceServiceInterface {
  getByUIdAndUserId(userId: number, uId: string): Promise<Device | null>;
  create(device: Device): Promise<Device>;
  deleteById(id: number, userId: number): Promise<number>;
  deleteFcmTokenById(id: number, userId: number): Promise<[number]>;
}
