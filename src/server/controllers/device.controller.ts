import { NextFunction, Request, Response } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Device } from '../models/device';
import { RequestWithBody, RequestWithParams, RequestWithPhoneNumber } from '../models/request-validation';
import { DeviceServiceInterface } from '../services/device.interface';
import { UserServiceInterface } from '../services/user.service';

@injectable()
export class DeviceController {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<DeviceServiceInterface>())
    private _deviceService: DeviceServiceInterface,
    @inject(nameof<UserServiceInterface>())
    private _userService: UserServiceInterface,
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DeviceController>());
  }

  async getByUIdAndPhoneNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithParams<Device>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
          params: request.validatedParams,
        },
      });
    }

    res.json(
      await this._deviceService.getByUIdAndUserId(
        user.id,
        request.validatedParams.uId
      )
    );
    next();
  }

  async create(req: Request, res: Response, next: NextFunction): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithBody<Device>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
          body: request.validatedBody,
        },
      });
    }
    const device = request.validatedBody;
    device.userId = user.id;
    res.json(await this._deviceService.create(device));
    next();
  }

  async deleteDevice(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithParams<Device>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
          params: request.validatedParams,
        },
      });
    }
    res.json({
      deleted: await this._deviceService.deleteById(
        request.validatedParams.id,
        user.id
      ),
    });
    next();
  }

  async deleteDeviceToken(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    const request = req as RequestWithPhoneNumber & RequestWithParams<Device>;
    const user = await this._userService.getByPhoneNumber(
      request.validatedPhoneNumber
    );
    if (!user) {
      throw this._error.getFormattedError({
        source: 'extapi',
        status: 400,
        message: 'Cannot find user by phoneNumber',
        errorData: {
          phoneNumber: request.validatedPhoneNumber,
          params: request.validatedParams,
        },
      });
    }
    res.json({
      updated: (
        await this._deviceService.deleteFcmTokenById(
          request.validatedParams.id,
          user.id
        )
      )[0],
    });
    next();
  }
}
