import { inject, injectable } from 'inversify';

import { Device } from '../models/device';
import { DeviceDataAccessInterface } from '../repository/device.interface';
import { DeviceServiceInterface } from './device.interface';

@injectable()
export class DeviceService implements DeviceServiceInterface {
  constructor(
    @inject(nameof<DeviceDataAccessInterface>())
    private _deviceDataAccess: DeviceDataAccessInterface
  ) {}

  async getByUIdAndUserId(userId: number, uId: string): Promise<Device | null> {
    return this._deviceDataAccess.getByUserIdAndUId(userId, uId);
  }

  async create(device: Device): Promise<Device> {
    const existingDevice = await this._deviceDataAccess.getByUserIdAndUId(
      device.userId,
      device.uId
    );

    if (existingDevice) {
      if (device.fcmToken && device.fcmToken != existingDevice.fcmToken) {
        const updatedFcmToken = await this._deviceDataAccess.updateDeviceFcmToken(
          existingDevice.id,
          device.fcmToken
        );
        if (updatedFcmToken[0]) {
          existingDevice.fcmToken = device.fcmToken;
          existingDevice.setDataValue('fcmToken', device.fcmToken);
        }
      }
      if (device.locale && device.locale != existingDevice.locale) {
        const updatedLocale = await this._deviceDataAccess.updateDeviceLocale(
          existingDevice.id,
          device.locale
        );
        if (updatedLocale[0]) {
          existingDevice.locale = device.locale;
          existingDevice.setDataValue('locale', device.locale);
        }
      }
      return existingDevice;
    }

    return this._deviceDataAccess.create(device);
  }
  async deleteById(id: number, userId: number): Promise<number> {
    return this._deviceDataAccess.deleteById(id, userId);
  }

  async deleteFcmTokenById(
    id: number,
    userId: number
  ): Promise<[number]> {
    return this._deviceDataAccess.deleteFcmTokenById(id, userId);
  }
}
