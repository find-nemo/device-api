import { Request } from 'express';
import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { RequestValidationMiddleware } from '../../plugins/request-validation.plugin';
import { RequestWithBody, RequestWithParams, RequestWithPhoneNumber } from '../models/request-validation';
import { RequestValidationMiddlewareType } from '../models/types';
import { User } from '../models/user';

@injectable()
export class ValidateTokenUtil {
  private _error: InterfaceErrorHandlerPlugin;
  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<RequestValidationMiddleware>());
  }

  validateTokenPhoneNumberFromUserModel(
    req: Request,
    reqOption: RequestValidationMiddlewareType
  ): void {
    if (reqOption === 'params') {
      const request = req as RequestWithPhoneNumber & RequestWithParams<User>;
      if (
        request.validatedPhoneNumber !== request.validatedParams.phoneNumber
      ) {
        throw this._error.getFormattedError({
          source: 'int',
          status: 401,
          message:
            'User token phone number does not matches with the provided phone number',
          errorData: {
            params: request.validatedParams,
            phoneNumber: request.validatedPhoneNumber
          }
        });
      }
    } else if (reqOption === 'body') {
      const request = req as RequestWithPhoneNumber & RequestWithBody<User>;
      if (request.validatedPhoneNumber !== request.validatedBody.phoneNumber) {
        throw this._error.getFormattedError({
          source: 'int',
          status: 401,
          message:
            'User token phone number does not matches with the provided phone number',
          errorData: {
            params: request.validatedBody,
            phoneNumber: request.validatedPhoneNumber
          }
        });
      }
    }
  }
}
