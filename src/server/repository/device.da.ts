import { inject, injectable } from 'inversify';

import { InterfaceErrorHandlerPlugin } from '../../plugins/error-handler.interface';
import { Device } from '../models/device';
import { DeviceDataAccessInterface } from './device.interface';

@injectable()
export class DeviceDataAccess implements DeviceDataAccessInterface {
  private _error: InterfaceErrorHandlerPlugin;

  constructor(
    @inject(nameof<InterfaceErrorHandlerPlugin>())
    errorFactory: (name: string) => InterfaceErrorHandlerPlugin
  ) {
    this._error = errorFactory(nameof<DeviceDataAccess>());
  }

  async getByUserIdAndUId(userId: number, uId: string): Promise<Device | null> {
    try {
      return await Device.findOne({
        where: {
          uId,
          userId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while getting devices by userId and uId',
        source: 'intdb',
        errorData: {
          error,
          userId,
          uId,
        },
      });
    }
  }

  async create(device: Device): Promise<Device> {
    try {
      if (device.save) {
        return await device.save();
      }
      return await Device.create(device);
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while creating device',
        source: 'intdb',
        errorData: {
          error,
          device,
        },
      });
    }
  }

  async updateDeviceFcmToken(
    deviceId: number,
    fcmToken: string
  ): Promise<[number]> {
    try {
      return await Device.update({ fcmToken }, { where: { id: deviceId } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating device fcmToken',
        source: 'intdb',
        errorData: {
          error,
          deviceId,
          fcmToken,
        },
      });
    }
  }

  async updateDeviceLocale(
    deviceId: number,
    locale: string
  ): Promise<[number]> {
    try {
      return await Device.update({ locale }, { where: { id: deviceId } });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while updating device locale',
        source: 'intdb',
        errorData: {
          error,
          deviceId,
          locale,
        },
      });
    }
  }

  async deleteById(id: number, userId: number): Promise<number> {
    try {
      return await Device.destroy({
        where: {
          id,
          userId,
        },
      });
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting deviceById',
        source: 'intdb',
        errorData: {
          error,
          id,
          userId,
        },
      });
    }
  }

  async deleteFcmTokenById(
    id: number,
    userId: number
  ): Promise<[number]> {
    try {
      return await Device.update(
        { fcmToken:  null },
        {
          where: {
            id,
            userId,
          },
        }
      );
    } catch (error) {
      throw this._error.getFormattedError({
        status: 500,
        message: 'Error while deleting deviceFcmTokenById',
        source: 'intdb',
        errorData: {
          error,
          id,
          userId,
        },
      });
    }
  }
}
