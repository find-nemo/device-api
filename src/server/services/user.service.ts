import { User } from '../models/user';

export interface UserServiceInterface {
  getByPhoneNumber(phoneNumber: string): Promise<User | null>;
}
