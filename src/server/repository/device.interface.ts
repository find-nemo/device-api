import { Device } from '../models/device';

export interface DeviceDataAccessInterface {
  getByUserIdAndUId(userId: number, uId: string): Promise<Device | null>;
  updateDeviceFcmToken(
    deviceId: number,
    fcmToken: string
  ): Promise<[number]>;
  updateDeviceLocale(
    deviceId: number,
    locale: string
  ): Promise<[number]>;
  create(device: Device): Promise<Device>;
  deleteById(id: number, userId: number): Promise<number>;
  deleteFcmTokenById(id: number, userId: number): Promise<[number]>;
}
