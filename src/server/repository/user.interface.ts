import { User } from '../models/user';

export interface UserDataAccessInterface {
  getByPhoneNumber(phoneNumber: string): Promise<User | null>;
}
