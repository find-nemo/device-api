import { Config } from '../server/models/config';

export const developmentConfig: Config = {
  serviceConfig: {
    name: 'Device API Microservice',
    environment: 'development',
    namespace: 'device-api',
    host: 'device-api.stg.bharatride.in',
    description: 'Presentation api to call internal microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'rock-verbena-322706',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
    },
    firestoreCollection: 'stagingTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.stg.bharatride.in',
    deviceApi: 'https://device-api.stg.bharatride.in',
  },
};
