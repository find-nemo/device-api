import { inject, injectable } from 'inversify';

import { User } from '../models/user';
import { UserDataAccessInterface } from '../repository/user.interface';
import { UserServiceInterface } from './user.service';

@injectable()
export class UserService implements UserServiceInterface {
  constructor(
    @inject(nameof<UserDataAccessInterface>())
    private _userDataAccess: UserDataAccessInterface
  ) {}

  async getByPhoneNumber(phoneNumber: string): Promise<User | null> {
    return this._userDataAccess.getByPhoneNumber(phoneNumber);
  }
}
