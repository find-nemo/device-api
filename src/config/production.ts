import { Config } from '../server/models/config';

export const productionConfig: Config = {
  serviceConfig: {
    name: 'Device API Microservice',
    environment: 'production',
    namespace: 'device-api',
    host: 'device-api.prod.bharatride.in',
    description: 'Presentation api to call internal microservices',
    port: 5555,
  },
  googleCloudConfig: {
    projectId: 'ride-api-prod',
    secrets: {
      database: 'database',
      firebase: 'firebase-service-account',
    },
    firestoreCollection: 'productionTimestamp',
  },
  endpoints: {
    userApi: 'https://user-api.prod.bharatride.in',
    deviceApi: 'https://device-api.prod.bharatride.in',
  },
};
