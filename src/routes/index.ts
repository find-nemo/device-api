import { Router } from 'express';

import { appContainer } from '../inversify.config';
import { InterfaceRequestValidationMiddleware } from '../plugins/request-validation.interface';
import { DeviceController } from '../server/controllers/device.controller';
import { HealthController } from '../server/controllers/health.controller';
import { SwaggerController } from '../server/controllers/swagger.controller';
import { Device } from '../server/models/device';

const routes = Router();

const swaggerController = appContainer.get<SwaggerController>(
  nameof<SwaggerController>()
);
const healthController = appContainer.get<HealthController>(
  nameof<HealthController>()
);

const deviceController = appContainer.get<DeviceController>(
  nameof<DeviceController>()
);
const requestValidationMiddleware = appContainer.get<
  InterfaceRequestValidationMiddleware
>(nameof<InterfaceRequestValidationMiddleware>());

const validationMiddleware = requestValidationMiddleware.validationMiddleware.bind(
  requestValidationMiddleware
);

routes.get('/', healthController.getHealth.bind(healthController));

routes.get('/health', healthController.getHealth.bind(healthController));

routes.get('/api', swaggerController.getDocs.bind(swaggerController));

/**
 * @swagger
 * /device/uId/{uId}/:
 *  get:
 *   description: Get a device for a user by uId
 *   summary: Returns device object
 *   tags: [Device]
 *   parameters:
 *     - name: uId
 *       description: Device uId
 *       in: path
 *       required: true
 *       schema:
 *        type: string
 *   responses:
 *     200:
 *       description: Get a device for a user
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeviceResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.get(
  '/device/uId/:uId/',
  [
    validationMiddleware('params', Device, {
      validator: { groups: ['getByUIdAndPhoneNumber'] },
    }),
  ],
  deviceController.getByUIdAndPhoneNumber.bind(deviceController)
);

/**
 * @swagger
 * /device/:
 *  post:
 *   description: Create a device for a user
 *   summary: Returns device object
 *   tags: [Device]
 *   requestBody:
 *     description: Device body
 *     required: true
 *     content:
 *       application/json:
 *        schema:
 *          $ref: '#/components/schemas/CreateDeviceBody'
 *   responses:
 *     200:
 *       description: Returns device object
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeviceResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.post(
  '/device/',
  [
    validationMiddleware('body', Device, {
      validator: { groups: ['createDevice'] },
    }),
  ],
  deviceController.create.bind(deviceController)
);

/**
 * @swagger
 * /device/{id}/:
 *  delete:
 *   description: Delete a device for a user by id
 *   summary: Returns number of device deleted
 *   tags: [Device]
 *   parameters:
 *     - name: id
 *       description: Device id
 *       in: path
 *       required: true
 *       schema:
 *        type: integer
 *   responses:
 *     200:
 *       description: Returns the number of device deleted for a user
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/DeleteRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  '/device/:id/',
  [
    validationMiddleware('params', Device, {
      validator: { groups: ['deleteDeviceById'] },
    }),
  ],
  deviceController.deleteDevice.bind(deviceController)
);

/**
 * @swagger
 * /device/{id}/fcm-token/:
 *  delete:
 *   description: Delete a device token for a user by id
 *   summary: Returns number of device deleted
 *   tags: [Device]
 *   parameters:
 *     - name: id
 *       description: Device id
 *       in: path
 *       required: true
 *       schema:
 *        type: integer
 *   responses:
 *     200:
 *       description: Returns the number of device updated for a user
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/UpdateRecordsResponse'
 *     default:
 *       description: Error response
 *       content:
 *         application/json:
 *          schema:
 *            $ref: '#/components/schemas/ErrorResponse'
 */
routes.delete(
  '/device/:id/fcm-token/',
  [
    validationMiddleware('params', Device, {
      validator: { groups: ['deleteDeviceTokenById'] },
    }),
  ],
  deviceController.deleteDeviceToken.bind(deviceController)
);
export { routes };
